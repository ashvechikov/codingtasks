CREATE DATABASE IF NOT EXISTS `codingtasks`;
USE `codingtasks`;

DROP TABLE IF EXISTS `Category`;

CREATE TABLE `Category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Category_Task`;

CREATE TABLE `Category_Task` (
  `Category_id` bigint(20) NOT NULL,
  `tasks_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_iev840kan8o5chh4cbtrle141` (`tasks_id`),
  KEY `FK_42802p41cw24ds9oscopxvjm5` (`Category_id`),
  CONSTRAINT `FK_42802p41cw24ds9oscopxvjm5` FOREIGN KEY (`Category_id`) REFERENCES `Category` (`id`),
  CONSTRAINT `FK_iev840kan8o5chh4cbtrle141` FOREIGN KEY (`tasks_id`) REFERENCES `Task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Task`;
CREATE TABLE `Task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `approved` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `taskLevel_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_s1tv08vhi1t0t2smfa76bq0hs` (`taskLevel_id`),
  KEY `FK_ciyqa1iwrp1949hxsfq9pife4` (`category_id`),
  CONSTRAINT `FK_ciyqa1iwrp1949hxsfq9pife4` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`),
  CONSTRAINT `FK_s1tv08vhi1t0t2smfa76bq0hs` FOREIGN KEY (`taskLevel_id`) REFERENCES `TaskLevel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `TaskLevel`;
CREATE TABLE `TaskLevel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `mnemo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `Category` VALUES (7,'String type tasks','String'),(8,'New input-output','NIO'),(9,'String type ','String'),(10,'sdfgsdfg','asdfgdf');
INSERT INTO `TaskLevel` VALUES (4,1,'level.noob'),(5,2,'level.not.noob'),(6,3,'level.middle'),(7,4,'level.advanced'),(8,5,'level.god');
