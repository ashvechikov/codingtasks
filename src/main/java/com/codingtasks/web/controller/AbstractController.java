package com.codingtasks.web.controller;

import com.codingtasks.core.service.CategoryService;
import com.codingtasks.core.service.TaskLevelService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public abstract class AbstractController {

    protected Logger log = Logger.getLogger(getClass());

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    protected TaskLevelService taskLevelService;

    @Autowired
    protected CategoryService categoryService;

    public String getMessage(String code) {
        return getMessage(code, null);
    }

    public String getMessage(String code, Object[] objects){
        try{
            return messageSource.getMessage(code, objects, getLocale());
        } catch (Exception e){
            return code;
        }
    }

    protected Locale getLocale() {
        return RequestContextUtils.getLocale(request);
    }

    public Cookie getCookie(String name){
        Cookie[] cookies = request.getCookies();
        if(cookies == null || StringUtils.isEmpty(name)){
            return null;
        }
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals(name)){
                return cookie;
            }
        }
        return null;
    }

    @ModelAttribute
    public void menuActiveClass() {
        request.setAttribute("menuActive", getPageLink());
    }

    public abstract String getPageLink();
}
