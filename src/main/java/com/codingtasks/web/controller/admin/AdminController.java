package com.codingtasks.web.controller.admin;

import com.codingtasks.core.Constants;
import com.codingtasks.web.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by home1 on 05.08.14.
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    public String unspecified() {
        return "admin/admin";
    }

    @Override
    public String getPageLink() {
        return Constants.MenuClassActive.ADMIN_HOME;
    }
}
