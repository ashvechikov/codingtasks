package com.codingtasks.web.controller.admin;

import com.codingtasks.core.Constants;
import com.codingtasks.core.model.TaskLevel;
import com.codingtasks.web.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by home1 on 05.08.14.
 */
@Controller
@RequestMapping("/admin/taskLevels")
public class AdminTaskLevelController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView load(@ModelAttribute TaskLevel taskLevel) {
        return getTaskLevelModel(taskLevel);
    }

//    @RequestMapping(method = RequestMethod.POST)
//    public ModelAndView addCategory(@Valid TaskLevel taskLevel, BindingResult bindingResult) {
//        if (bindingResult.hasErrors()) {
//            return getTaskLevelModel(taskLevel);
//        }
//
//        taskLevelService.save(taskLevel);
//        return new ModelAndView("admin/taskLevels");
//    }

    private ModelAndView getTaskLevelModel(TaskLevel taskLevel) {
        ModelAndView model = new ModelAndView("admin/taskLevels");
        model.addObject("levels", taskLevelService.findAll());
        model.addObject("taskLevel", taskLevel);
        return model;
    }

    @Override
    public String getPageLink() {
        return Constants.MenuClassActive.ADMIN_TASK_LEVELS;
    }
}
