package com.codingtasks.web.controller.admin;

import com.codingtasks.core.Constants;
import com.codingtasks.core.model.Category;
import com.codingtasks.core.service.TaskService;
import com.codingtasks.utils.ajax.JsonResponse;
import com.codingtasks.utils.ajax.JsonResponseStatus;
import com.codingtasks.web.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AdminCategoryController extends AbstractController {

    private final static String CATEGORY_MAPPING_URL = "admin/categories";

    @RequestMapping(value = "/" + CATEGORY_MAPPING_URL, method = RequestMethod.GET)
    public ModelAndView load(@ModelAttribute Category category) {
        return getCategoryModel(category);
    }

    @RequestMapping(value = "/admin/category/add", method = RequestMethod.POST)
    public ModelAndView addCategory(@Valid Category category, BindingResult bindingResult) {
        System.out.println("1");
        if (bindingResult.hasErrors()) {
            return getCategoryModel(category);
        }
        System.out.println("2");
        categoryService.save(category);

        return getCategoryModel(new Category());
    }

    @RequestMapping(value = "/admin/category/remove", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody JsonResponse removeCategory(long categoryId) {
        try {
            categoryService.delete(categoryId);
        } catch (Exception e) {
            log.error("Category " + categoryId + " couldn't be deleted! " + e.getMessage());

            return new JsonResponse(JsonResponseStatus.ERROR,
                    "Category has inner tasks, so it couldn't be deleted until has either one task!");
        }

        return new JsonResponse(JsonResponseStatus.SUCCESS);
    }

    private ModelAndView getCategoryModel(Category category) {
        ModelAndView model = new ModelAndView(CATEGORY_MAPPING_URL);
        model.addObject("categories", categoryService.findAll());
        model.addObject("category", category);
        return model;
    }

    @Override
    public String getPageLink() {
        return Constants.MenuClassActive.ADMIN_CATEGORIES;
    }
}
