package com.codingtasks.web.controller.admin;

import com.codingtasks.core.Constants;
import com.codingtasks.core.model.Task;
import com.codingtasks.core.service.TaskService;
import com.codingtasks.utils.ajax.JsonResponse;
import com.codingtasks.utils.ajax.JsonResponseStatus;
import com.codingtasks.web.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by shved90
 * Date: 15.11.14
 * Time: 13:08
 */
@Controller
public class AdminTasksController extends AbstractController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/admin/tasks", method = RequestMethod.GET)
    public ModelAndView load(@ModelAttribute Task task) {
        return getTaskModel(task);
    }

//    @RequestMapping(value = "/admin/task/add", method = RequestMethod.POST)
//    public ModelAndView addCategory(@Valid Task task, BindingResult bindingResult) {
//        if (bindingResult.hasErrors()) {
//            return getTaskModel(task);
//        }
//        taskService.save(task);
//
//        return new ModelAndView("admin/tasks");
//    }

    @RequestMapping(value = "/admin/task/remove", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody JsonResponse removeTask(long taskId) {
        try {
            taskService.delete(taskId);
        } catch (Exception e) {
            log.error("Task " + taskId + " couldn't be deleted! " + e.getMessage());
            return new JsonResponse(JsonResponseStatus.ERROR, "Error occured");
        }
        return new JsonResponse(JsonResponseStatus.SUCCESS);
    }

    private ModelAndView getTaskModel(Task task) {
        ModelAndView model = new ModelAndView("admin/tasks");
        model.addObject("tasks", taskService.findAll());
        model.addObject("task", task);
        return model;
    }

    @Override
    public String getPageLink() {
        return Constants.MenuClassActive.ADMIN_TASKS;
    }
}
