package com.codingtasks.web.controller;

import com.codingtasks.core.Constants;
import com.codingtasks.core.model.Task;
import com.codingtasks.core.service.TaskService;
import com.codingtasks.utils.ajax.JsonResponse;
import com.codingtasks.utils.ajax.JsonResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by home1 on 04.08.14.
 */
@Controller
public class NewTaskController extends AbstractController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(method = RequestMethod.GET, value = "/newtask")
    public ModelAndView load(@ModelAttribute Task task) {
        return new ModelAndView("newtask", "task", task);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/newtask")
    public ModelAndView addNewTask(@Valid @ModelAttribute Task task, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView("newtask");
        model.addObject("task", task);
        if (bindingResult.hasErrors()) {
            return model;
        }

        taskService.save(task);
        model.addObject("succesfull",Boolean.TRUE);
        return model;
    }

    @Override
    public String getPageLink() {
        return Constants.MenuClassActive.NEW_TASK;
    }
}
