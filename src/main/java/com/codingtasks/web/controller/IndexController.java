package com.codingtasks.web.controller;

import com.codingtasks.core.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class IndexController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView load() {
        ModelAndView model = new ModelAndView("index");
        model.addObject("levels", taskLevelService.findAll());
        model.addObject("categories", categoryService.findAll());

        return model;
    }

    @Override
    public String getPageLink() {
        return Constants.MenuClassActive.HOME;
    }
}