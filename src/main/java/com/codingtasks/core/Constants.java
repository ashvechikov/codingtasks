package com.codingtasks.core;

/**
 * Created by home1 on 12.09.14.
 */
public final class Constants {

    public static final class MenuClassActive {
        public static final String ADMIN_HOME        = "home";
        public static final String ADMIN_CATEGORIES  = "categories";
        public static final String ADMIN_TASKS       = "tasks";
        public static final String ADMIN_TASK_LEVELS = "taskLevels";

        public static final String HOME              = "home";
        public static final String NEW_TASK          = "newtask";
    }

}
