package com.codingtasks.core.service;

import com.codingtasks.core.model.Category;
import com.codingtasks.core.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Created by home1 on 04.08.14.
 */

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    @Transactional(readOnly = true)
    public Category findOne(long id) {
        return categoryRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    @Transactional
    public Category save(Category category) {
        Assert.notNull(category);
        return categoryRepository.save(category);
    }

    @Override
    public void delete(Long categoryId) {
        categoryRepository.delete(categoryId);
    }
}
