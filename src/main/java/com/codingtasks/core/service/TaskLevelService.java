package com.codingtasks.core.service;

import com.codingtasks.core.model.TaskLevel;

/**
 * Created by home1 on 04.08.14.
 */
public interface TaskLevelService {

    Iterable<TaskLevel> findAll();
    TaskLevel findOne(long id);

    void save(TaskLevel taskLevel);
}
