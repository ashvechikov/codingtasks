package com.codingtasks.core.service;

import com.codingtasks.core.model.Task;
import com.codingtasks.core.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Created by home1 on 04.08.14.
 */
@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Iterable<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Task> findAll(Pageable pageable) {
        return taskRepository.findAllCommon(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Task> findAllForAdmin(Pageable pageable) {
        return taskRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Task findOne(long id) {
        return taskRepository.findOne(id);
    }

    @Override
    public void save(Task task) {
        Assert.notNull(task);
        taskRepository.save(task);
    }

    @Override
    public void delete(long id) {
        taskRepository.delete(id);
    }
}
