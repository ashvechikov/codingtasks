package com.codingtasks.core.service;

import com.codingtasks.core.model.Category;

/**
 * Created by home1 on 04.08.14.
 */
public interface CategoryService {

    Category findOne(long id);
    Iterable<Category> findAll();

    Category save(Category category);
    void delete(Long categoryId);
}
