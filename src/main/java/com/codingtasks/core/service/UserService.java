package com.codingtasks.core.service;

import com.codingtasks.core.model.User.User;

/**
 * Created by home1 on 06.08.14.
 */
public interface UserService {

    void save(User user);
    User findOne(Long id);
}
