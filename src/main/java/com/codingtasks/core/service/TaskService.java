package com.codingtasks.core.service;

import com.codingtasks.core.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by home1 on 04.08.14.
 */
public interface TaskService {

    Iterable<Task> findAll();
    Page<Task> findAll(Pageable pageable);
    Page<Task> findAllForAdmin(Pageable pageable);

    Task findOne(long id);
    void save(Task task);
    void delete(long id);
}
