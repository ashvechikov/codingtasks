package com.codingtasks.core.service;

import com.codingtasks.core.model.TaskLevel;
import com.codingtasks.core.repository.TaskLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Created by home1 on 04.08.14.
 */

@Service
public class TaskLevelServiceImpl implements TaskLevelService {

    @Autowired
    private TaskLevelRepository taskLevelRepository;

    @Override
    @Cacheable("TaskLevels")
    @Transactional(readOnly = true)
    public Iterable<TaskLevel> findAll() {
        return taskLevelRepository.findAll();
    }

    @Override
    @Cacheable("TaskLevels")
    @Transactional(readOnly = true)
    public TaskLevel findOne(long id) {
        return taskLevelRepository.findOne(id);
    }

    @Override
    @Transactional
    public void save(TaskLevel taskLevel) {
        Assert.notNull(taskLevel);
        taskLevelRepository.save(taskLevel);
    }
}
