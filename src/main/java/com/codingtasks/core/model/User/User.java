package com.codingtasks.core.model.User;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.security.Principal;

/**
 * Created by home1 on 06.08.14.
 */
@Entity
@Data
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String login;

    @NotEmpty @Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})")
    private String password;

    @Enumerated
    private UserRole userRole;
}
