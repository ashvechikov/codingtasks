package com.codingtasks.core.model;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by home1 on 04.08.14.
 */
@Entity
@Data
public class Task {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private TaskLevel taskLevel;

    @NotNull @NotEmpty
    private String name;
    @NotNull @NotEmpty
    private String description;

    private String tags;

    @OneToOne(cascade = CascadeType.DETACH)
    private Category category;

    private boolean approved;
    private boolean deleted;
}
