package com.codingtasks.core.model;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by home1 on 04.08.14.
 */
@Entity
@Data
public class TaskLevel {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull @Min(1)
    private Integer level;

    @NotNull @NotEmpty
    private String mnemo;
}
