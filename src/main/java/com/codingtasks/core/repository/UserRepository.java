package com.codingtasks.core.repository;

import com.codingtasks.core.model.User.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by home1 on 06.08.14.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
