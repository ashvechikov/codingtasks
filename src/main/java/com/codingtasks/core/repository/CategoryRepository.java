package com.codingtasks.core.repository;

import com.codingtasks.core.model.Category;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by home1 on 04.08.14.
 */
@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
}
