package com.codingtasks.core.repository;

import com.codingtasks.core.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by home1 on 04.08.14.
 */
@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

    @Query("from Task t where t.deleted = 0 AND t.approved = 1")
    Page<Task> findAllCommon(Pageable pageable);
}
