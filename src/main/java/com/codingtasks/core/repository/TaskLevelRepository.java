package com.codingtasks.core.repository;

import com.codingtasks.core.model.TaskLevel;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by home1 on 04.08.14.
 */
@Repository
public interface TaskLevelRepository extends CrudRepository<TaskLevel, Long> {
}
