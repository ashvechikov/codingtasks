package com.codingtasks.utils;

public class Time {

    public static int MINUTE = 60;
    public static int HOUR = MINUTE * 60;
    public static int DAY = HOUR * 24;
    public static int WEEK = DAY * 7;

}
