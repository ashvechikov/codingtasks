package com.codingtasks.utils;

import org.springframework.util.NumberUtils;

public class ParseUtils {

    public static int parseInt(String str) {
        return parseNumber(str, Integer.class, 0);
    }

    public static int unsafeParseInt(String str) {
        return parseNumber(str, Integer.class, null);
    }

    public static double parseDouble(String str) {
        return parseNumber(str, Double.class, 0d);
    }

    public static double unsafeParseDouble(String str) {
        return parseNumber(str, Double.class, null);
    }

    public static <T extends Number> T parseNumber(String str, Class<T> clazz, T defaultValue) {
        try {
            return NumberUtils.parseNumber(str, clazz);
        } catch (Exception e) {
            if(defaultValue == null) {
                throw new NumberFormatException();
            }
            return defaultValue;
        }
    }

}
