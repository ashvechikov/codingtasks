package com.codingtasks.utils;

import lombok.extern.log4j.Log4j;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j
public class HtmlUtils {
	private static Pattern urlPatternInText = null;
	private static Pattern emailPatternInText = null;

	private final static String hostNamePatt = "[\\w\\d]+[\\w\\d\\-]*[\\w\\d]+";
	private final static String portValuePatt = "[\\d]{1,5}";
	private final static String dirNamePatt = "([\\w\\d\\-_!\\*'\\.\\$&\\+=\\?@]|%[a-fA-F\\d]{2})";
	private final static String queryVariablePatt = "([\\w\\d\\-_!\\*'\\(\\)\\.\\$\\+,:;@/]|%[a-fA-F\\d]{2})";
	private final static String anchorNamePatt = "([\\w\\d\\-_!\\*'\\(\\)\\.\\$&\\+,:;=\\?@/]|%[a-fA-F\\d]{2})";
	private final static String emailNamePatt = "\\w[\\w\\d\\-\\._]*";
	private final static String urlPattStr = "https?://(" + hostNamePatt + "\\.)+" + hostNamePatt +
		"(:" + portValuePatt + ")?(/(" + dirNamePatt + ")+)*/?(\\?" + queryVariablePatt + "+=" +
		queryVariablePatt + "*(\\&" + queryVariablePatt + "+=" + queryVariablePatt + "*)*)?(#" +
		anchorNamePatt + ")?";
	private final static String emailPattStr = emailNamePatt + "@(" + hostNamePatt + "\\.)+" + hostNamePatt;

	private final static String begin = "(^|\\s|[(.,:>])";
	
	public static String addHyperlinks(String str) {
		try {
			str = str.replaceAll("\r", "").replaceAll("\n", "<br />");
			Pattern pattern = getEmailPatternInText();
	        Matcher matcher = pattern.matcher(str);
	        StringBuffer buffer = new StringBuffer();
	        while (matcher.find()) {
	        		matcher.appendReplacement(buffer, matcher.group(1) + "<a target=_blank href='mailto:" +
	        			matcher.group(2).trim()+"'>"+matcher.group(2).trim()+"</a>");
	        }
	        matcher.appendTail(buffer);
	        str = buffer.toString();
			pattern = getUrlPatternInText();
	        matcher = pattern.matcher(str);
	        buffer = new StringBuffer();
	        while (matcher.find()) {
	        		matcher.appendReplacement(buffer, matcher.group(1) + "<a target=_blank href='" +
	        			matcher.group(2).trim()+"'>"+matcher.group(2).trim()+"</a>");
	        }
	        matcher.appendTail(buffer);
	        str = buffer.toString();
		} catch (Exception e) {
//			log.error(e.getMessage(), e);
		}
		return str;
	}
	public static Pattern getEmailPatternInText() {
		if (emailPatternInText == null) {
			emailPatternInText = Pattern.compile(begin + "(" + emailPattStr + ")");
		}
		return emailPatternInText;
	}

	public static Pattern getUrlPatternInText() {
		if (urlPatternInText == null) {
			urlPatternInText = Pattern.compile(begin + "("+urlPattStr+")");
		}
		return urlPatternInText;
	}
}
