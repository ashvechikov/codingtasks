package com.codingtasks.utils.ajax;

public enum JsonResponseStatus {

    SUCCESS, ERROR;

}
