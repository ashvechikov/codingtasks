<%@ include file="/WEB-INF/include/pageHead.jsp"%>


<div class="container">
    <c:if test="${not empty categories}">
        <table class="table table-hover">
            <thead>
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Description</td>
                <td></td>
                <td><input type="checkbox" onchange="selectAllCategories($(this).is(':checked'))" /> </td>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="category" items="${categories}" varStatus="index">
                <tr>
                    <td>${index.index+1}</td>
                    <td>${category.name}</td>
                    <td>${category.description}</td>
                    <td><img class="delete-btn" onclick="Categories.removeCategory(${category.id}, this);i--"/></td>
                    <td><input type="checkbox" name="categories" id="category${category.id}"/> </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <form:form modelAttribute="category" action="/admin/category/add" method="post">
        <c:if test="${not empty status}">
            <div class="error">Please, specify all fields!</div>
        </c:if>
        <div class="form-group">
            <label>Name</label>
            <form:input path="name" cssClass="form-control"/>
            <form:errors path="name">You should specify name!</form:errors>
        </div>
        <div class="form-group">
            <label>Description</label>
            <form:textarea path="description" cssClass="form-control"></form:textarea>
            <form:errors path="description">You should specify description!</form:errors>
        </div>
        <form:button type="submit" class="btn btn-primary">Save category</form:button>
    </form:form>
</div>

