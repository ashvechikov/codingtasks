<%@ include file="/WEB-INF/include/pageHead.jsp"%>


<div class="container">
    <c:if test="${not empty tasks}">
        <table class="table table-hover">
            <thead>
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Task level</td>
                <td>Description</td>
                <td>Tags</td>
                <td>Approved</td>
                <td>Deleted</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="task" items="${tasks}" varStatus="index">
                <tr>
                    <td>${index.index + 1}</td>
                    <td>${task.name}</td>
                    <td>${task.taskLevel.mnemo}</td>
                    <td>${task.description}</td>
                    <td>
                        <c:choose>
                            <c:when test="${task.approved == true}">
                                Approved
                            </c:when>
                            <c:otherwise>
                                Not approved
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:if test="${task.deleted == true}">
                            Deleted
                        </c:if>
                    </td>
                    <td><img class="delete-btn" onclick="Tasks.removeTask(${task.id}, this)"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <%--<form:form modelAttribute="task" action="/admin/addtask" method="post">--%>
        <%--<c:if test="${not empty status}">--%>
            <%--<div class="error">Please, specify all fields!</div>--%>
        <%--</c:if>--%>
        <%--<div class="form-group">--%>
            <%--<label>Name</label>--%>
            <%--<form:input path="name" cssClass="form-control"/>--%>
            <%--<form:errors path="name">You should specify name!</form:errors>--%>
        <%--</div>--%>
        <%--<div class="form-group">--%>
            <%--<label>Description</label>--%>
            <%--<form:textarea path="description" cssClass="form-control"></form:textarea>--%>
            <%--<form:errors path="description">You should specify description!</form:errors>--%>
        <%--</div>--%>
        <%--<form:button type="submit" class="btn btn-primary">Save category</form:button>--%>
    <%--</form:form>--%>
</div>


