<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<div class="container">
    <c:if test="${not empty levels}">
        <table class="table table-hover">
            <thead>
            <tr>
                <td>#</td>
                <td>Level</td>
                <td>Description</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="taskLevel" items="${levels}" varStatus="index">
                <tr>
                    <td>${index.index + 1}</td>
                    <td>${taskLevel.level}</td>
                    <td><spring:message code="${taskLevel.mnemo}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <%--<form:form modelAttribute="taskLevel" action="/admin/addlevel" method="post">--%>
        <%--<c:if test="${not empty status}">--%>
            <%--<div class="error">Please, specify all fields!</div>--%>
        <%--</c:if>--%>
        <%--<div class="form-group">--%>
            <%--<label>Level</label>--%>
            <%--<form:input path="level" cssClass="form-control"/>--%>
            <%--<form:errors path="level"/>--%>
        <%--</div>--%>
        <%--<div class="form-group">--%>
            <%--<label>Mnemo (need to add to properties or db - decide later what is better)</label>--%>
            <%--<form:textarea path="mnemo" cssClass="form-control"></form:textarea>--%>
            <%--<form:errors path="mnemo"/>--%>
        <%--</div>--%>
        <%--<form:button type="submit" class="btn btn-primary">Save Task Level</form:button>--%>
    <%--</form:form>--%>
</div>