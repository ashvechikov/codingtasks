<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<html>
<head>
    <title>Add new task</title>
    <script src="/resources/js/newtask.js"></script>
</head>
<body>
    <c:choose>
        <c:when test="${success}">
            You succesfully added new task!
            <%
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(400);
                        } catch (InterruptedException e) {}
                    }
                }).start();
            %>
        </c:when>
        <c:otherwise>
            <form:form modelAttribute="task" action="/newtask" name="task">
                <label>Name</label><form:input path="name"/>
                <label>Description</label><form:input path="description"/>
                <label>Tags</label><form:input path="tags"/>
                <select>
                    <c:if test="${not empty levels}">
                        <option value="0"/>
                        <c:forEach items="${levels}" var="level">
                            <option value="${level.level}"><spring:message code="${level.mnemo}"/></option>
                        </c:forEach>
                    </c:if>
                </select>

                <form:button value="Send task"/>
            </form:form>
        </c:otherwise>
    </c:choose>

</body>
</html>
