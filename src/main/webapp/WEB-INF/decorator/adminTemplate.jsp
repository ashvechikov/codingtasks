<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="icon" href="/resources/images/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<c:url value='/resources/bootstrap/css/bootstrap.min.css'/>" type="text/css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/styles.css'/>" type="text/css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/styles.css'/>" type="text/css" rel="stylesheet" />
    <script src="<c:url value='/resources/js/jquery-2.1.1.min.js'/>" type="application/javascript"></script>
    <script src="<c:url value='/resources/js/admin.js'/>" type="application/javascript"></script>
    <script src="<c:url value='/resources/bootstrap/js/bootstrap.min.js'/>" type="application/javascript"></script>

    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>
</head>
<body>
    <div class="navbar-wrapper">
        <div class="container">

            <div class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Coding tasks</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li id="home"><a href="/admin">Home</a></li>
                            <li id="categories"><a href="/admin/categories">Categories</a></li>
                            <li id="tasks"><a href="/admin/tasks">Tasks</a></li>
                            <li id="taskLevels"><a href="/admin/taskLevels">Task levels</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div>
        <sitemesh:write property='body'/>
    </div>

    <!-- FOOTER -->
    <footer>
        <p class="footer"><spring:message code="copyright"/></p>
    </footer>
</body>

<script type="application/javascript">
    $(function() {
        $('#${menuActive}').addClass('active');
    });
</script>
</html>