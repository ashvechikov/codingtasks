<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="icon" href="/resources/images/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<c:url value='/resources/bootstrap/css/bootstrap.min.css'/>" type="text/css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/styles.css'/>" type="text/css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/styles.css'/>" type="text/css" rel="stylesheet" />
    <script src="<c:url value='/resources/js/jquery-2.1.1.min.js'/>" type="application/javascript"></script>
    <script src="<c:url value='/resources/js/functions.js.js'/>" type="application/javascript"></script>
    <script src="<c:url value='/resources/bootstrap/js/bootstrap.min.js'/>" type="application/javascript"></script>

    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>
</head>
<body>

<div class="navbar-wrapper">
    <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Coding tasks</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li id="home"><a href="/">Home</a></li>
                        <li id="about"><a href="/about">About</a></li>
                        <li id="contact"><a href="/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>

    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
            <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
            <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
            <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
    </div>

    <hr class="featurette-divider">
    <sitemesh:write property='body'/>

    <!-- /END THE FEATURETTES -->

    <!-- FOOTER -->
    <footer>
        <p class="pull-right"><spring:message code="copyright"/></p>
    </footer>

</div>

        <%--<div class="header">--%>
            <%--<span class="main-text">--%>
                <%--Coding tasks--%>
            <%--</span>--%>
            <%--<c:if test="${not empty categories}">--%>
                <%--<span class="select-span">--%>
                    <%--<label>Choose category</label>--%>
                        <%--<select>--%>
                            <%--<option value="0"/>--%>
                            <%--<c:forEach items="${categories}" var="category">--%>
                                <%--<option value="${category.id}" >${category.name}</option>--%>
                            <%--</c:forEach>--%>
                        <%--</select>--%>
                <%--</span>--%>
            <%--</c:if>--%>
            <%--<span class="select-span">--%>
                <%--<label>Choose level</label>--%>
                <%--<c:if test="${not empty levels}">--%>
                    <%--<select>--%>
                        <%--<option value="0"/>--%>
                        <%--<c:forEach items="${levels}" var="level">--%>
                            <%--<option value="${level.level}"><spring:message code="${level.mnemo}"/></option>--%>
                        <%--</c:forEach>--%>
                    <%--</select>--%>
                <%--</c:if>--%>
            <%--</span>--%>
        <%--</div>--%>
</body>

<script type="application/javascript">
    $(function() {
        $('#${menuActive}').addClass('active');
    });
</script>
</html>