var i = 0;

function selectAllCategories(value) {
    $("input[name='categories']").each(function() {
        this.checked = value;
    });
}



Categories = {

    removeCategory: function(id, item,categories,index) {
        console.log($(item).parent().parent().index);
        if (confirm("Do you really want to delete this task?")) {
            $.ajax({
                type: "POST",
                url: "/admin/category/remove",
                data: { categoryId: id },
                dataType: "json",
                success: function(data) {
                    if (data.status == "SUCCESS") {
                        $(item).parent().parent().remove();
                        location.reload();
                    } else {
                        alert(data.result);
                    }
                }
            });
        }
    }
}

Tasks = {
    removeTask: function(id, item) {
        if (confirm("Do you really want to delete this task?")) {
            $.ajax({
                type: "POST",
                url: "/admin/task/remove",
                data: { taskId: id },
                dataType: "json",
                success: function (data) {
                    if (data.status == "SUCCESS") {
                        $(item).parent().parent().remove();
                    } else {
                        alert(data.result);
                    }
                }
            });
        }
    }
}