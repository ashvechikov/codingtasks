package com.codingtasks.service;

import com.codingtasks.core.model.Category;
import com.codingtasks.core.model.Task;
import com.codingtasks.core.service.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml", "classpath:mvc-dispatcher-servlet.xml"})
@WebAppConfiguration
@TransactionConfiguration
@Transactional
public class TestCategoryService {

    @Autowired
    private CategoryService categoryService;

    private CategoryService mockCategoryService;

    @Before
    public void setUp() throws Exception {
        mockCategoryService = mock(CategoryService.class);

        Category category1 = new Category();
        category1.setId(1L);
        category1.setDescription("cat1");
        category1.setName("IO");
        category1.setTasks(Collections.<Task>emptyList());

        Category category2 = new Category();
        category1.setId(2L);
        category1.setDescription("cat2");
        category1.setName("IO");
        category1.setTasks(Collections.<Task>emptyList());

        Category category3 = new Category();
        category1.setId(3L);
        category1.setDescription("cat3");
        category1.setName("IO");
        category1.setTasks(Collections.<Task>emptyList());

        when(mockCategoryService.findAll()).thenReturn(Arrays.asList(category1, category2, category3));
    }

    @Test
    public void testFirstElemFromFindAll() {
        Category category =  ((List<Category>)categoryService.findAll()).get(0);
        assertEquals(Long.valueOf(11l), category.getId());
        assertEquals("asdasf", category.getName());
        assertEquals("asdfsdf", category.getDescription());
        assertEquals(0, category.getTasks().size());
    }

    @Test
    public void testFindAllNotEmpty() {
        assertTrue(((List<Category>)categoryService.findAll()).size() > 0);
    }

    @Test
    public void testFindOne() {
        Category category =  categoryService.findOne(11l);
        assertNotNull(category);
        assertEquals("asdasf", category.getName());
        assertEquals("asdfsdf", category.getDescription());
    }

    @Test
    public void testMockFindAllSize() {
        assertEquals(3, ((List)mockCategoryService.findAll()).size());
    }
}

